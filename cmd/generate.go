/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"text/template"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

var temp *template.Template
var inFile string = "slurm_tmpl.txt"
var outFile string = "job.slurm"

// generateCmd represents the generate command
var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "generate slurm file",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("generate called")
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)
	temp = template.Must(template.ParseFiles(inFile))
	//j := defJob()
	j := readYaml()
	//	j.StartA = 0
	//	j.EndA = 999
	printJob(j)
	file, err := os.Create(outFile)
	check(err)
	err = temp.Execute(file, j)
	check(err)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// generateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// generateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func defJob() Job {
	j := Job{
		Name:      "slurmTest",
		Account:   "group-rci",
		Partition: "unsafe",
		Module:    "Python/3.10",
		Exefile:   "$HOME/hello.py",
		Program:   "Python",
		EOfile:    "test",
		Cpus:      2,
		Mem:       2000,
		Nodes:     1,
		Time:      10}
	return j
}

func check(e error) {
	if e != nil {
		log.Fatalln(e)
	}
}

func printJob(j Job) {
	fmt.Printf("Name      : %s\n", j.Name)
	fmt.Printf("Out & Err : %s\n", j.EOfile)
	fmt.Printf("Account   : %s\n", j.Account)
	fmt.Printf("Partition : %s\n", j.Partition)
	fmt.Printf("Module    : %s\n", j.Module)
	fmt.Printf("Execute   : %s\n", j.Exefile)
	if j.EndA != j.StartA {
		fmt.Printf("Array     : [%d-%d]\n", j.StartA, j.EndA)
	}
	fmt.Printf("cpus : %d\n", j.Cpus)
	fmt.Printf("mem  : %d MB\n", j.Mem)
	fmt.Printf("time : %d Min\n\n", j.Time)
}

func readYaml() Job {

	yfile, err := ioutil.ReadFile("job.yaml")

	if err != nil {

		log.Fatal(err)
	}

	//var data Job
	data := make(map[string]Job)

	err2 := yaml.Unmarshal(yfile, &data)

	if err2 != nil {

		log.Fatal(err2)
	}
	return data["job1"]
}
