package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v3"
)

type Job struct {
	Name      string
	Account   string
	Partition string
	Module    string
	Exefile   string
	Program   string
	EOfile    string
	Cpus      int
	Mem       int
	Nodes     int
	Time      int
	StartA    int
	EndA      int
}

func printJob(j Job) {
	fmt.Printf("Name      : %s\n", j.Name)
	fmt.Printf("Out & Err : %s\n", j.EOfile)
	fmt.Printf("Account   : %s\n", j.Account)
	fmt.Printf("Partition : %s\n", j.Partition)
	fmt.Printf("Module    : %s\n", j.Module)
	fmt.Printf("Execute   : %s\n", j.Exefile)
	if j.EndA != j.StartA {
		fmt.Printf("Array     : [%d-%d]\n", j.StartA, j.EndA)
	}
	fmt.Printf("cpus : %d\n", j.Cpus)
	fmt.Printf("mem  : %d MB\n", j.Mem)
	fmt.Printf("time : %d Min\n\n", j.Time)
}

func main() {

	yfile, err := ioutil.ReadFile("job.yaml")

	if err != nil {

		log.Fatal(err)
	}

	//var data Job
	data := make(map[string]Job)

	err2 := yaml.Unmarshal(yfile, &data)

	if err2 != nil {

		log.Fatal(err2)
	}
	fmt.Println(data)
	for j := range data {
		printJob(data[j])
	}
}
